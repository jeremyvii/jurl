/**
 * A wrapper for exposing the JUrl class to the browser window object. This
 * method allows the class to be accessed without the new keyword.
 *
 * @package    JUrl
 * @copyright  2017 Jeremy Jackson. All rights reserved
 * @license    GNU Lesser General Public License v3 (LGPL-3.0).
 */

(function(global) {
  "use strict";
  // Tell browserify to get our JUrl class
  let module = require('./jurl.ts');
  // Expose JUrl to the window object
  (<any>global).JUrl = (url: string) => {
    return new module.JUrl(url);
  }
})(window);
