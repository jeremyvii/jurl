/**
 * A lightweight library for manipulating a URL string
 *
 * @package    JUrl
 * @copyright  2017 Jeremy Jackson. All rights reserved
 * @license    GNU Lesser General Public License v3 (LGPL-3.0).
 */

/**
 * Object definition for URLs passed to JUrl
 */
interface Url {
  [key: string]: string,
  protocol: string,
  hostname: string,
  port:     string,
  pathname: string,
  search:   string,
  hash:     string,
  host:     string
}

/**
 * Object definition for the URL query object
 */
interface Query {
  [x: string]: any;
}

/**
 * A lightweight library TypeScript for manipulating a URL string
 *
 * JUrl takes a URL string and converts it into an object (@see Query), to be
 * manipulated by the API methods JUrl provides.
 */
export class JUrl {
  /**
   * Parsed URL object
   *
   * @var {Url}
   */
  public url: Url;

  /**
   * Entry point for JUrl
   *
   * Takes URL provided and parses it as a "Url" object
   *
   * @param  {string} url The URL to be parsed
   */
  constructor(url: string) {
    this.url = this.parse(url);
  }

  /**
   * Appends to query string based on key/value provided
   *
   * Gets current URL query via getURLQuery() and appends new property to
   * query object. The query is then converted to a string via
   * buildQueryString() and applied to the url's search property
   *
   * @see    buildQueryString()
   * @see    getURLQuery()
   *
   * @param  {string} key   New query's name
   * @param  {any}    value New query's value
   *
   * @return {object}       Returns the current object context (this), to
   *                        perserve chainablitiy
   */
  public addQuery(key: string, value: any): object {
    // Get current URL query
    let query = this.getURLQuery();
    // Append new query to current URL query object
    query[key] = value;
    // Apply the updated query object to the URL's search proptery
    this.queryToString(query);
    // Perserve chainablitiy
    return this;
  }

  /**
   * Check if provided arrays are equal
   *
   * First a check is made to see if parameters provided are arrays. If so the
   * lengths of the arrays are compared to save time. If they have the same
   * length the first array is iterated over. A check for nested arrays via
   * `isArray()` is made, if any are found a recursive call is made to compare
   * those array values. If those values don't match, false is returned. If no
   * multi-dimensional arrays are found, a standard value comparison is made.
   * If any values don't match false is returned. Otherwise true is returned at
   * the end.
   *
   * @see    isArray()
   *
   * @param  {Array<any>} array1 First array to compare
   * @param  {Array<any>} array2 Second array to compare
   *
   * @return {boolean}           True if arrays are equal, otherwise false
   */
  public arrayEquals(array1: Array<any>, array2: Array<any>): boolean {
    // Make sure that both arrays given are actually array
    if (!this.isArray(array1) || !this.isArray(array2))
      return false;
    // Compare lengths before we compare values
    if (array1.length !== array2.length)
      return false;
    // Iterate over first array
    for (let i = 0; i < array1.length; i++) {
      // Check if arrays are multi-dimensional
      if (this.isArray(array1[i]) || this.isArray(array2[i])) {
        // Recursively compare
        if (!this.arrayEquals(array1[i], array2[i])) return false
      // Compare array values
      } else if (array1[i] !== array2[i]) return false;
    }
    return true;
  }

  /**
   * Builds a URL query string based on object provided
   *
   * First the `list` parameter is checked to see if it is an object, otherwise
   * we return false. Then a query string is created with the beginning `?`.
   * The `list` is then iterated over and another check is done to see if the
   * property exits on the object. The property in `list` is checked to see if
   * it is an array. If so the array is iterated over and the key/value is URL
   * encoded and appending to query string. If the property is not an array,
   * the key/value is encoded and appended to the string. The string is then
   * returned.
   *
   * @see    isArray()
   *
   * @param  {Query} list Object to convert to a URL
   *
   * @return {mixed}      URL query string, Return false if param provided
   *                      isn't an object
   */
  public buildQueryString(list: Query): string | boolean {
    // Check if item is an object
    if (Object.prototype.toString.call(list) === '[object Object]') {
      // Create empty string for the building the query
      let query = '?';
      // Loop over object so we can implode results into a string
      for (let property in list) {
        // Make sure has this property
        if (list.hasOwnProperty(property)) {
          // Get property's value
          let value = list[property];
          // Check if value is an array
          if (this.isArray(value)) {
            // Indicate array with URL query
            for (let i = 0; i < value.length; i++)
              // Create URL query string as array string. Ex: ?test[]=1&test[]=2
              query += encodeURI(property) + '[]=' + encodeURI(value[i]) + '&';
          } else {
            // Create URL query string and append to query string
            query += encodeURI(property) + '=' + encodeURI(value) + '&';
          }
        }
      }
      // Return built query string and remove ending "&"
      return query.slice(0, -1);
    }
    return false;
  }

  /**
   * Gets URL query value based on key provided
   *
   * `getURLQuery()` is called to get all the Query values. A check is done to
   * see if this property exists in the Query. If not, false is returned,
   * otherwise, the property's value is returned.
   *
   * @see    getURLQuery()
   *
   * @param  {string} key Property to get value for
   *
   * @return {any}        Value for property provided
   */
  public getQuery(key: string): any {
    // Get URL query object
    let query = this.getURLQuery();
    // Throw error if key doesn't exist on the Query object
    if (!query.hasOwnProperty(key)) return false;
    // Return the query value
    return query[key];
  }

  /**
   * Get URL querys and returns them as key value object
   *
   * A blank Query object is created by default. This will be the base for the
   * return value. Then the Search property on the url object is checked to see
   * if it has data. If it is empty the blank Query object is returned.
   * Otherwise the beginning `?` of the URL query is removed and an array is
   * created out of the Search property, delimited by a `&`. The query list is
   * the looped over. The key is tested for an ending `[]`, indicated the value
   * should be an array. Then the query object is checked to see if the
   * property has already been defined. If it has been defined, the value is
   * then appened to the array. Otherwise a new array is created for this
   * property. If the key does not have the ending `[]` the key/value is simply
   * added to the Query object. Once the loop is finished the Query object is
   * returned
   *
   * @see    isArray()
   *
   * @return {Query} URL Query object
   */
  public getURLQuery(): Query {
    // Create query object
    let query: Query = {};
    // Check if URL query is empty before continuing
    if (!this.url.search) return query;
    // Get list of url query's. Each array element is formatted like: key=value
    let queryList = this.url.search.substr(1).split('&');
    // Loop over query list
    for (let i = 0; i < queryList.length; i++) {
      // Split array item into array with 0 as key and 1 as value
      let item = queryList[i].split('=');
      // Define key and value
      let key = item[0];
      let value = item[1];
      // Define regular expression for checking if key is for an array
      // (Ex: `test[]`)
      const regex = /[\[\]]{2}$/;
      // Check if value is for an array by checking key for brackets
      if (regex.test(key)) {
        // Remove trailing `[]` from the end of they key
        key = key.replace(regex, '');
        // Check if key is already defined on the query object
        if (query.hasOwnProperty(key)) {
          // Check if value is an array
          if (this.isArray(query[key])) {
            // Append value to array
            query[key].push(value);
          } else throw 'Error: property: ' + key + ' should be an array';
        } else {
          // Define query as an array with value appened
          query[key] = [value];
        }
      // Otherwise we will just append key/value to query object
      } else {
        // Append URL query to object
        query[key] = value;
      }
    }
    // Return query object
    return query;
  }

  /**
   * Checks if property and property value exist in URL Query
   *
   * Get's URL query object, then checks if `key` provided exists in that Query.
   * Afterwards a check if made to see if a `value` for the provided `key` has
   * been given. If not the result of `key` check is returned. If so a check is
   * made to see if the `value` provided is an array. If it is, `arrayEquals()`
   * is used to check if the URL Query value matches the array `value` given.
   * Returns true if the arrays match. If the value isn't an array, a strict
   * comparison is made and true is returned if the values match. Otherwise
   * false is returned at the end.
   *
   * @see    arrayEquals()
   *
   * @param  {string}  key   Query property to check for
   * @param  {any}     value Query property value to check for. (Optional)
   *
   * @return {boolean}       True if Query has key/value provided
   */
  public hasQuery(key: string, value: any = false): boolean {
    // Get current URL query
    let query = this.getURLQuery();
    // Check if key exists prior to value check
    let keyExists = query.hasOwnProperty(key);
    // Check if value has been given
    if (!value)
      // If value hasn't been given check for the existiance of the key
      // provided
      return keyExists;
    // Check if query has key provided
    if (keyExists) {
      // Check if value provided is an array
      if (this.isArray(value)) {
        // Compare arrays
        if (this.arrayEquals(value, query[key]))
          // Indicate that property and value specified are in the URL query
          return true;
      } else if (query[key] === value)
        // Indicate that property and value specified are in the URL query
        return true;
    }
    return false;
  }

  /**
   * Checks if variable is an array
   *
   * Checks if variable is an array by comparing the variable's constructor to
   * the Array constructor.
   *
   * @param  {Array<any>} array Variable to check
   *
   * @return {boolean}          True if variable is an array otherwise false
   */
  private isArray(array: Array<any>): boolean {
    return array.constructor === Array;
  }

  /**
   * Parses URL provided into an Url object
   *
   * First we define our parser as an undefined variable to prevent errors with
   * the compiler. Then a check to determine if the `document` object is
   * present. If not an error is thrown. Otherwisem a HTML anchor element is
   * created, `<a>Example</a>`. This will be used this to parse the URL. The
   * URL string provided this method will be applied to anchor's `href`
   * attribute. Afterwards we will be able to access the properties we need and
   * return them as a Query object.
   *
   * @param  {string} url Url to be parsed.
   *
   * @return {Url}        Parsed Url
   */
  public parse(url: string): Url {
    // Define our return value
    let parser;
    // Make sure we are in a web browser
    if (typeof document === 'undefined') {
      throw 'JUrl must be run in a web browser enviroment with a document.'
    } else {
      // Create anchor element
      parser = document.createElement('a');
      // Pass url provided to the anchor's href attribute
      parser.href = url;
    }
    // Define our return values
    return {
      protocol: parser['protocol'],
      hostname: parser['hostname'],
      port:     parser['port'],
      pathname: parser['pathname'],
      search:   parser['search'],
      hash:     parser['hash'],
      host:     parser['host']
    };
  }

  /**
   * Apply URL Query object to the URL search proptery
   *
   * Creates query string from the query parameter provided with
   * `buildQueryString()`. If `buildQueryString()` returns false, an error is
   * thrown. The query string to then applied to the search proptery of `url`.
   *
   * @param  {Query} query Query object to apply to search filter
   *
   * @return {void}
   */
  public queryToString(query: Query): void {
    // Build query string with new URL query value
    let queryString = this.buildQueryString(query);
    // Throw error is query is false
    if (queryString === false) throw 'Invalid query string!';
    // Add query string to URL
    this.url.search = queryString.toString();
  }

  /**
   * Removes URL query from URL's search property via the key provided
   *
   * Get's URL's search property as a `Query` object. The key provided is then
   * checked to see if it exists on the query. The search query is then removed
   * from the query object. The query object is converted to a string and
   * applied to the search property via `queryToString()`. `this` is returned
   * to allow this method to be chainable.
   *
   * @see    queryToString()
   *
   * @param  {string} key The URL query property to remove
   *
   * @return {object}     `this` to perserve chainablitiy
   */
  public removeQuery(key: string): object {
    // Get current URL query
    let query = this.getURLQuery();
    // Throw error if query doesn't exist
    if (!query.hasOwnProperty(key))
      throw 'Error: query doesn\'t exist in URL.';
    // Delete property from URL query
    delete query[key];
    // Apply the updated query object to the URL's search proptery
    this.queryToString(query);
    // Perserve chainablitiy
    return this;
  }

  /**
   * Converts `Url` object to a URL string
   *
   * @return {string}  `Url` object converted to a string
   */
  public toString(): string {
    // Create blank string for our URL we will return
    let url = '';
    // Define our allowed properties
    let properties = ['protocol', 'hostname', 'port', 'pathname', 'search',
      'hash'];
    // Iterate over URL properties
    for (let property of properties) {
      // Store property value to save time
      let value: string = '';
      // Check if property exits
      if (this.url[property]) {
        // Prevent errors by converted value to string
        value = this.url[property].toString();
      } else {
        // Don't append value to string if undefined
        continue;
      }
      // Add special case for protocol since it doesn't include the `//`
      if (property === 'protocol') {
        // Append protocol to URL
        url += value + '//';
      } else if (property === 'port') {
        // Prepend colon to port number
        url += ':' + value;
      } else {
        // Append property value to URL
        url += value;
      }
    }
    return url;
  }
}
