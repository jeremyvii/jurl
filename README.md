# JUrl - JavaScript URL Parser

A lightweight library TypeScript for manipulating a URL string.

JUrl takes a URL string and converts it into an object, to be manipulated by
the API methods JUrl provides.

## Usage

``` JavaScript
// Just pass in your URL
// No new keyword needed!
let url = JUrl('https://google.com?test=hello');

// Get a string reprensentation of your URL via the toString() method
// Returns: https://google.com?search=hello
url.toString();

// Add a new URL query with addQuery()
url.addQuery('query', 'value');

// Get the URL queries as an object via getURLQuery()
let query = url.getURLQuery();
// Logs 'hello'
console.log(query.test);

// You can start over with a completely new URL via the parse method.
url.parse(window.location);

// Applies the previous URL query to your current URL query
url.search = url.buildQueryString(query)
// Logs '?test=hello&query=value'
console.log(url.search);
```

## Methods

|Name                    |Description                                                      |
|------------------------|-----------------------------------------------------------------|
|`addQuery(key, value)`  |Appends to query string based on key/value provided              |
|`buildQueryString(list)`|Builds URL query string based on object provided                 |
|`getQuery(key)`         |Gets URL query value based on key provided                       |
|`getURLQuery()`         |Get URL querys and returns them as key value object              |
|`hasQuery(key, value?)` |Checks if proptery and proptery value exist in URL Query         |
|`parse(url)`            |Parses URL provided into an Url object                           |
|`removeQuery(key)`      |Removes URL query from URL's search property via the key provided|
|`toString()`            |Convert `Url` object to a URL string                             |

For full documentation and list of methods view the [generated TypeDoc](http://jeremyvii.com/jurl).



## Build

Builds are managed with [gulp](https://gulpjs.com/). To compile JUrl for use in
the browser run:

``` shell
gulp
```

To build as module run:

``` shell
gulp module
```

Note: Building JUrl as a module requires you to use the `new` constructor
syntax. When you export JUrl as a module you only exporting the class in
`jurl.ts`, not the `wrapper.ts`.

``` JavaScript
let url = new JUrl(urlString);
```

To build [TypeDoc](http://typedoc.org/) documentation run:

``` shell
gulp typedoc
```

## Node.js

JUrl's builds and dependencies are managed with node.js, however JUrl is built
for browser use. I recommend using the [URL API](https://nodejs.org/api/url.html)
provided by node.js when in that environment.
