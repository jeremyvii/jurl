const gulp       = require('gulp');
const ts         = require('gulp-typescript');
const tsProject  = ts.createProject('tsconfig.json');
const typedoc    = require('gulp-typedoc');
const del        = require('del');
const browserify = require('browserify');
const tsify      = require('tsify');
const source     = require('vinyl-source-stream');
const buildFile  = 'jurl.js';
const buildDir   = 'build';

/**
 * Compiles the files provided and places them into the build folder
 */
function compile(files) {
  return browserify({
    entries: files
  }).plugin(tsify)
    .bundle()
    .pipe(source(buildFile))
    .pipe(gulp.dest(buildDir));
}

// The default build task
// Compiles both the wrapper and jurl files. Then a build directory is created
// and with the compile JavaScript
gulp.task('default', () => {
  return compile([
    'src/jurl.ts',
    'src/wrapper.ts'
  ]);
});

// Deletes out the build directory
gulp.task('clean', () => {
  return del(['build/']);
});

// Compiles just the jurl file for modular purposes
gulp.task('module', () => {
  let tsProject = ts.createProject('tsconfig.json', { module: 'commonjs' });
  return tsProject.src()
    .pipe(tsProject())
    .js.pipe(gulp.dest(buildDir));
});

// Compiles the TypeDoc documentation
gulp.task('typedoc', () => {
  return gulp.src(["src/jurl.ts"]).pipe(typedoc({
    module: "ES6",
    target: "es5",
    out: "docs/",
    name: "JUrl"
  }));
});
